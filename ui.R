##### COVID19 shinyapps ######
## country comparison and data plot
## created and developped by Denis Mongin (@denis_mongin)
## see https://scitilab.shinyapps.io/Covid19/ and scitilab.com
## If you use or adapt, Please cite by refering to my website or twitter account
## CC BY-NC-SA

library(leaflet)
library(lubridate)
library(data.table)
library(shiny)
library(sf)
library(plotly)
library(ggplot2)
library(dplyr)
library(RColorBrewer)
library(shinyalert)
library(shinyjs)


# load the managed data, see download_hopkins.R file
load("Covid19.RData")

ui <- fluidPage(
  useShinyalert(),#set up shinyalert
  useShinyjs(),# set up Shinyjs to make things disappear
  # fluidRow(column(6,verbatimTextOutput("debug")),column(6,verbatimTextOutput("debug2"))), # debug
  titlePanel(
    h1("Covid-19: country comparison", align = "center"),
    windowTitle = "Covid-19 per country"
  ),
  helpText("Click country on the map to select or deselect. Plotting options below. More in 'about'. Designed by Denis Mongin"),
  fluidRow(column(4,offset = 6,actionButton("about", "About"))), # abot button
  tabsetPanel(id = "tabs",
              tabPanel("Cumulative cases", 
                       fluidRow(
                         column(6,
                                fluidRow(leafletOutput(outputId = "mymap"))
                         ),
                         column(6,
                                fluidRow(plotlyOutput('plot1'))
                         )
                       )
              ), 
              tabPanel("Daily cases",
                       fluidRow(
                         column(6,
                                fluidRow(leafletOutput(outputId = "mymap_2"))
                         ),
                         column(6,
                                fluidRow(plotlyOutput('plot2'))
                         )
                       )
              ),
              tabPanel("Cumulative deaths",
                       fluidRow(
                         column(6,
                                fluidRow(leafletOutput(outputId = "mymap_3"))
                         ),
                         column(6,
                                fluidRow(plotlyOutput('plot3'))
                         )
                       )
              ),
              tabPanel("Daily deaths",
                       fluidRow(
                         column(6,
                                fluidRow(leafletOutput(outputId = "mymap_4"))
                         ),
                         column(6,
                                fluidRow(plotlyOutput('plot4'))
                         )
                       )
              )
  ),
  fluidRow(
    column(6,
           fluidRow(
             column(8,offset = 2,sliderInput("Datemap",
                                             h3("Situation date for the map"),
                                             min = datemin,
                                             max = datemax,
                                             value=datemax)))
    ),
    column(6,
           fluidRow(column(4,checkboxGroupInput("plot_cond", label = h4("Plotting style"), 
                                                choices = list("Log scale" = "log", 
                                                               "Days since N cases" = "sincefirst"),
                                                selected = "log")),
                    column(3,  numericInput("firstcase", label = h4("N cases"), value = 10,min = 0,max = 1000)),
                    column(4, offset = 1,radioButtons("var", label = h4("Cases"),
                                                      choices = list("absolute number" = "cases", "Population proportion" = "pcases"),
                                                      selected = "cases"))
           )
    )
  ),
  
  HTML('<footer>  @denis_mongin  </footer>')
)






